/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamunch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author root
 */
public class Datamunch {

    static FileArrayProvider fap = new FileArrayProvider();
    static String[] accounts;
    static String[] resources;
    static File file;
    static Integer resourceCount, nextResource = 0;
    //static String pattern = "src='(.*?)'";
    static String pattern = "https?://(?:[a-z\\-]+\\.)+[a-z]{2,6}(?:/[^/#?]+)+\\.(?:jpg|gif|png|mp4)";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        accounts = fap.readLines("./accounts.txt");
        resources = fap.readLines("./resources.txt");
        file = new File("./saved_log.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        resourceCount = resources.length;
        System.out.println("Starting Prosses");
        if (resources.length > 0) {
            for (String account : accounts) {
                getSrcFromURL(getResource(), account);
            }
        }
    }

    public static void savelog(String input) throws IOException {
        FileWriter fileWritter = new FileWriter(file.getName(), true);
        try (BufferedWriter bufferWritter = new BufferedWriter(fileWritter)) {
            bufferWritter.write(input+"\n");
        }
    }

    public static void getSrcArray(String input, String account) {
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(input);
        while (m.find()) {
            try {
                saveFile(m.group(0), account);
            } catch (IOException ex) {
                System.out.println("Failed to Fetch");
                //Logger.getLogger(Datamunch.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static String getResource() {
        Integer returnResource = nextResource;
        if ((nextResource + 1) >= resourceCount) {
            nextResource = 0;
        } else {
            nextResource++;
        }
        return resources[returnResource];
    }

    public static void getSrcFromURL(String webURL, String username) throws IOException {
        System.out.println("Fetching: " + webURL + username);
        Document doc = Jsoup.connect(webURL + username).get();
        getSrcArray(doc.toString(), username);
//        Elements media = doc.select("[src]");
//        media.stream().filter((src) -> (src.tagName().equals("img"))).forEach((src) -> {
//            try {
//                String filename = URLReplace(src.attr("abs:src"));
//                saveFile(filename, username);
//            } catch (IOException ex) {
//                System.out.println("Failed to Fetch");
//                //Logger.getLogger(Datamunch.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        });
    }

    public static String URLReplace(String fileURL) {
        String filename = fileURL.replaceAll("_s.jpg", "_n.jpg");
        filename = filename.replaceAll("_a.jpg", "_n.jpg");
        return filename;
    }

    public static boolean checkFolderExists(String folder) {
        File theFolder = new File(folder);
        if (!theFolder.exists()) {
            System.out.println("Creating Folder : " + folder);
            try {
                theFolder.mkdir();
                System.out.println("Folder Created");
                return true;
            } catch (SecurityException se) {
                System.out.println("Folder Creation Failed");
                return false;
            }
        }
        return true;
    }

    public static void saveFile(String fileUrl, String folder) throws IOException {
        if (checkFolderExists(folder)) {
            URL url = new URL(fileUrl);
            String fileName = url.getFile();
            String destName = "./" + folder + fileName.substring(fileName.lastIndexOf("/"));
            File f = new File(destName);
            if (!f.isFile()) {
                System.out.println("Saving file: " + destName);
                savelog(destName);
                InputStream is = url.openStream();
                OutputStream os = new FileOutputStream(destName);
                byte[] b = new byte[2048];
                int length;
                while ((length = is.read(b)) != -1) {
                    os.write(b, 0, length);
                }
                is.close();
                os.close();
            } else {
                System.out.println("File :" + destName + " Already Exists");
            }
        } else {
            System.out.println("Save file Failed");
        }
    }

}
